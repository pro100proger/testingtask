<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Список клиентов</title>
</head>
<body>
	<h1>Список клиентов</h1>
	<ul>
		<c:forEach var="contact" items="${contacts}">
			<li><a href="${contact.url}">${contact.name}</a></li>
		</c:forEach>
	</ul>
	<a href="person?add">Добавить клиента</a></a>
</body>
</html>