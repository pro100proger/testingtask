<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Редактирование</title>
</head>
<body>
	<h1>Редактирование информации о клиенте</h1>
	<form action="person" method="post">
	<input type="hidden" name="edit">
	<input type="hidden" name="id" value="${person.id}">
	<ul>
		<li>Имя: <input type="text" name="name" value="${person.name}"></li>
		<li>Фамилия: <input type="text" name="surname" value="${person.surname}"></li>
		<li>Отчество: <input type="text" name="middle" value="${person.middle}"></li>
	</ul>
	<input type="submit" value="Редактировать">
	</form>
	<form action="person" method="post">
	<input type="hidden" name="delete">
	<input type="hidden" name="id" value="${person.id}">
	<input type="submit" value="Удалить">
	</form>
	<a href="contacts"> <<Вернуться к списку</a>
</body>
</html>