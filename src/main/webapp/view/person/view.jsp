<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Клиент</title>
</head>
<body>
	<h1>Клиент</h1>
	<ul>
		<li>Имя: ${person.surname}</li>
		<li>Фамилия: ${person.name}</li>
		<li>Отчество: ${person.middle}</li>
	</ul>
	<a href="${person.url}&edit">Редактировать</a> |
	<a href="contacts"> <<Вернуться к списку</a>
</body>
</html>