package com.cft.contacts.representations;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import com.cft.contacts.entities.Person;

public class PersonRepresentation {

	public final String name;
	public final String surname;
	public final String middle;
	public final Map<String, URI> links = new HashMap<String, URI>();

	public PersonRepresentation(Person person) {
		this.name = person.getName();
		this.surname = person.getSurname();
		this.middle = person.getMiddle();
		try {
			links.put("self", new URI("/person/" + person.getId()));
			links.put("collection", new URI("/contacts"));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
}
