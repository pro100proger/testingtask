package com.cft.contacts.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cft.contacts.entities.Person;
import com.cft.contacts.repositories.PersonRepository;
import com.cft.contacts.representations.PersonRepresentation;

@Controller
public class PersonController {

	@Autowired
	private PersonRepository personRepository;

	@RequestMapping(value = "/person", params = "add", method = RequestMethod.GET)
	public String getAddPerson() {
		return "person/add";
	}

	@RequestMapping(value = "/person", params = "edit", method = RequestMethod.GET)
	public String getEditPerson(@RequestParam long id, Model model) {
		model.addAttribute("person", personRepository.findOne(id));
		return "person/edit";
	}

	@RequestMapping(value = "/person", method = RequestMethod.GET)
	public String getViewPerson(@RequestParam long id, Model model) {
		model.addAttribute("person", personRepository.findOne(id));
		return "person/view";
	}

	@RequestMapping(value = "/person/{id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody PersonRepresentation getPerson(@PathVariable long id) {
		return new PersonRepresentation(personRepository.findOne(id));
	}

	@RequestMapping(value = "/person", params = "add", method = RequestMethod.POST)
	public String postAddPerson(@RequestParam String name,
			@RequestParam String surname, @RequestParam String middle) {
		Person person = new Person(name, surname, middle);
		person = personRepository.save(person);

		return "redirect:person?id=" + person.getId();
	}

	@RequestMapping(value = "/person", params = "edit", method = RequestMethod.POST)
	// @Transactional
	public String postEditPerson(@RequestParam long id,
			@RequestParam String name, @RequestParam String surname,
			@RequestParam String middle) {
		personRepository.delete(id);
		Person person = new Person(name, surname, middle);
		person = personRepository.save(person);

		return "redirect:person?id=" + person.getId();
	}

	@RequestMapping(value = "/person", params = "delete", method = RequestMethod.POST)
	public String postDeletePerson(@RequestParam long id) {
		personRepository.delete(id);
		return "redirect:contacts";
	}

}
