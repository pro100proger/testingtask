package com.cft.contacts.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cft.contacts.entities.Person;

public interface PersonRepository extends JpaRepository<Person, Long> {
}
