package com.cft.contacts.entities;

import javax.persistence.Entity;

@Entity
public class Person extends Contact {

	String name;
	String surname;
	String middle;

	public Person() {
	}

	public Person(String name, String surname, String middle) {
		super(surname);
		this.name = name;
		this.surname = surname;
		this.middle = middle;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getMiddle() {
		return middle;
	}

	public void setMiddle(String middle) {
		this.middle = middle;
	}

}
